# Bash the Stack

This bash script provides an interface to a stateful stack which is held in the
shell's environment.

It provides a FORTH-like extendible syntax and high level toolset.

## Usage

```
$ source bts
$ bts.push [ token ]*
```

## Terminology

A 'token' is a string which either represents 'data' or 'words'.

A 'word' refers to a 'function' which is part of the stacks 'dictionary'.

A 'function' can be written as a shell functions or as a sequence of 'tokens'.

The 'dictionary' consists of a map of names which map to a shell function call.

It comes with a minimalist dictionary and can be extended arbirarily.

## Parsing Tokens

Tokens are pushed to the stack in left to right order. Each token is either
a 'word' in the dictionary or a value which is either an argument to a preceding
word or a numeric which will be placed directly on the stack.

## Example

By default, bts provides a crappy integer only calculator:

```
$ bts.push 30 2 / .
15
```

In this example, 30 is pushed to stack, followed by 2, then the dividor is
invoked which in turn takes the top two items of the stack and pushes its
result. The dot word simply displays and pops the top of the stack.

A list of the dictionary and each words definitions can be obtained from:

```
$ bts.dictionary
```

## Core Grammar

| Token                 | Description                                                 |
| --------------------- | ----------------------------------------------------------- |
| \<numeric\>           | push \<numeric\> to the top of the stack                    |
| $ \<string\>          | push \<string\> to the of the stack                         |
| .                     | display value of item at the top of the stack and remove it |
| +                     | add the top two values at the top of the stack              |
| *                     | multiply the top two values at the top of the stack         |
| /                     | divide the top two values at the top of the stack           |
| -                     | subtract the top two values at the top of the stack         |
| %                     | modulo the top two values at the top of the stack           |
| : \<name\> \<body\> ; | define a new word as \<name\> with \<body\>                 |
| forget \<name\>       | remove the previously defined name                          |
| depth                 | place the number of items in the stack at the top of stack  |
| clear                 | remove everything from the stack                            |
| \<n\> pick            | duplicate the \<n\>-th item at the top of the stack         |
| \<n\> roll            | move the \<n\>-th item at the top of the stack              |
| drop                  | drop the top of the stack

## Extending

The general underlying form to create a new word is:

```
$ bts.push : <name> [<type>] <body> \;
```

Where \<type\> is either unspecified (to indicate that the \<body\> consists of
a sequence of tokens), '--shell' to indicate that the \<body\> represents a shell
function or executable with output sent to stdout or '--capture' to indicate
that the \<body\> represents a shell function or executable, the stdout result
of which will be placed at the top of the stack.

To simplify, provides bts.def functions which have the form:

```
bts.def <name> <type> <body>
```

For example:

```
$ bts.def square dup \*
$ bts.push 10 square .
100
$ bts.def date --capture date
$ bts.push date .
Tue 18 Dec 17:55:30 CET 2018
$ bts.def_shell --shell date
Tue 18 Dec 17:55:30 CET 2018
```

The latter two showing the difference between def --capture and --shell - the
former placing the output on the stack (hence the . required to display it)
while the latter just outputs to stdout directly.
